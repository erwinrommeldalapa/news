# News Exam

News list REST Api that provides CRUD Operations.

### Tools and programming language used

javascript, jQuery, virtual machine (Vagrant), nginx server ubuntu, bootstrap 4.0, HTML5, css, PHP, json, Ajax, MySQL, Restlet Client DHC (for testing), git

### Installation Requirements

* MySQL / MariaDB for database 
* Browser Chrome / Mozilla
* nginx / apache server

### Installaation

In the utils/config.php change the value of the following 

private $host = "192.168.10.10";
private $username = "homestead"; //change to db connection username
private $password = "secret"; //change to db connection password

## index.php

Here users can view the news.

## news-post.php?id={id}

Here users can view the specific news with content, users can comment and delete comment on this page.

## {baseURL}/backend/index.php

Users can create a news in this page.

## {baseURL}/backend/newslist.php

Users can view, update and delete the news in this page.

## API Endpoints / usage
{baseURL}/api/create.php
METHOD: POST
params: {"title": "TEST TITLE", "body": "testt content body"}

{baseURL}/api/create-comment.php
METHOD: POST
params: {"news_id": 1, "comment": "good"}

{baseURL}/api/delete.php
METHOD: POST
params: {"id": 1}

{baseURL}/api/delete-comment.php
METHOD: POST
params: {"id" : 1}

{baseURL}/api/update.php
METHOD: POST
params: {"id" : 1, "title": "new title", "body": "new body content"}

{baseURL}/api/view.php
METHOD: GET

{baseURL}/news-post.php?id={id}
METHOD: GET
param: {baseURL}/news-post.php?id=3

## utils/CommentsController
Here users can see the sql query scripts for the comments.

## utils/NewsController
Here users can see the sql query scripts for the comments.

## Microframeworks used
SB Admin frontend assets

## Exam Requirements
Please create a website that has the following use-cases using PHP and MySQL.
The UI does not need to be fancy/interactive but you are welcome to do it.
 
 - create news - {baseURL}/api/create.php

 - delete news - {baseURL}/api/delete.php

 - view news - {baseURL}/api/view.php

 - create comment - {baseURL}/api/create-comment.php

 - delete comment - {baseURL}/api/delete-comment.php

 - view comments of a news - {baseURL}/news-post.php?id={id}

 