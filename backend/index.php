<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>News</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index-2.html">News</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

  </nav>

  <div id="wrapper">

     <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">
          <i class="far fa-newspaper"></i>
          <span>News</span>
        </a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="newslist.php">
          <i class="far fa-newspaper"></i>
          <span>News List</span>
        </a>
      </li>
    </ul>

    <div id="content-wrapper">
      <div class="container-fluid">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">News</a>
          </li>
        </ol>
        <div class="alert alert-success" role="alert" id="alertSuccess" style=" display:none;">
         You have successfully created a news.
       </div>
      </div>
      <div class="container">
        <div class="card card-login mx-auto mt-5">
          <div class="card-header">Create News</div>
          <div class="card-body">
            <form>
              <div class="form-group">
                <label for="title">Title:</label>
                <div class="form-label-group">
                  <input type="email" id="title" class="form-control" placeholder="News Title" required="required" autofocus="autofocus">
                </div>
              </div>
              <div class="form-group">
                <label for="content">Content:</label>
                <div class="form-label-group text-center">
                  <textarea name="message" rows="10" cols="30" id="body"> </textarea>
                </div>
              </div>
              <a class="btn btn-primary btn-block" href="#" id="btnCreateSubmit">Submit</a>
            </form>
          </div>
        </div>
      </div>
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span></span>
          </div>
        </div>
      </footer>
    </div>

  </div>
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script type="text/javascript">
      $(document).ready(function () {
        $("#btnCreateSubmit").click(function () {
          var param = {
            title: $("#title").val(),
            body: $("#body").val()
          };
          $.ajax({
            type: "POST",
            dataType: "json",
            url: "../api/create.php",
            contentType: "application/json; charset=utf-8",
            data:  JSON.stringify(param),
            success: function(data){
              console.log(data);
              $('#alertSuccess').show();
            },
            error: function(error){
              console.log("Error:");
              console.log(error);
            }
          });
        });

        $(".btnEdit").click(function () {
          $('#id').val($(this).data('id'));
          $('#texts').val($(this).data('texts'));
          $('#editTodo').modal('show');
        });

        $("#btnEditSubmit").click(function () {
          var param = {
            text: $("#texts").val(),
            id: $("#id").val()
          };
          $.ajax({
            type: "PUT",
            dataType: "json",
            url: "../api/update.php",
            contentType: "application/json; charset=utf-8",
            data:  JSON.stringify(param),
            success: function(data){
              console.log(data);
            },
            error: function(error){
              console.log("Error:");
              console.log(error);
            }
          });
        });
      });
    </script>

  <script src="js/sb-admin.min.js"></script>

</body>

</html>
