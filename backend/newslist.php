<!DOCTYPE html>
<html lang="en">
<?php
include_once "../utils/config.php";
include_once "../utils/NewsController.php";
$database = new Database();
$db = $database->getConnection();

$newslist = $db->prepare("SELECT * FROM news");
$newslist->execute();
?>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>News</title>

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="css/sb-admin.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.php">News List</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

  </nav>

  <div id="wrapper">

    <ul class="sidebar navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.php">
          <i class="far fa-newspaper"></i>
          <span>News</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="newslist.php">
          <i class="far fa-newspaper"></i>
          <span>News List</span>
        </a>
      </li>
    </ul>

    <div id="content-wrapper">
      <div class="container-fluid">
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">News List</a>
          </li>
        </ol>
      </div>
      <div class="alert alert-success" role="alert" id="alertSuccess" style=" display:none;">
       You have deleted a news.
     </div>
     <div class="alert alert-success" role="alert" id="alertSuccessUpdate" style=" display:none;">
      You have successfully updated a news.
    </div>
    <div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
      News List</div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>id</th>
                <th>Title</th>
                <th>Body</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Update</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <?php 
                foreach($newslist as $news){
                  echo "<tr>";
                  echo "<td>".$news['id']."</td>";
                  echo "<td>".$news['title']."</td>";
                  echo "<td>".$news['body']."</td>";
                  echo "<td>".$news['created_at']."</td>";
                  echo "<td>".$news['updated_at']."</td>";
                  ?>
                  <td>
                    <button type="button" 
                    data-id="<?php echo $news['id']; ?>"
                    data-text="edit"
                    data-title="<?php echo $news['title']; ?>"
                    data-body="<?php echo $news['body']; ?>"
                    class="btn btn-primary btnEdit" id="btnEdit">  
                    <i class="fa fa-edit" aria-hidden="true"></i>
                  </td>
                  <td>
                    <button type="button" 
                    data-id="<?php echo $news['id']; ?>"
                    data-texts="<?php echo $news['title']; ?>"
                    class="btn btn-primary btnDelete" id="btnDeleteSubmit-<?php echo $news['id']; ?>">  
                    <i class="fa fa-trash" aria-hidden="true"></i>
                  </td>
                  <?php
                }
                echo "</tr>";
                ?>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="container" id="updateForm" style="display: none;">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Update News ID  #<span id="updateID"></span></div>
        <input type="id" id="id" hidden>
        <div class="card-body">
          <div class="form-group">
            <label for="title">Title:</label>
            <div class="form-label-group">
              <input type="text" id="title" class="form-control" placeholder="News Title" required="required" autofocus="autofocus">
            </div>
          </div>
          <div class="form-group">
            <label for="content">Content:</label>
            <div class="form-label-group text-center">
              <textarea name="message" rows="10" cols="30" id="body"> </textarea>
            </div>
          </div>
          <button class="btn btn-primary btn-block" id="btnEditSubmit" data-text="editSubmit">Submit</button>
        </div>
      </div>
    </div>
    <footer class="sticky-footer">
      <div class="container my-auto">
        <div class="copyright text-center my-auto">
          <span></span>
        </div>
      </div>
    </footer>
  </div>

</div>
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>


<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {

    $("#btnCreateSubmit").click(function () {
      var param = {
        title: $("#title").val(),
        body: $("#body").val()
      };
      $.ajax({
        type: "POST",
        dataType: "json",
        url: "../api/create.php",
        contentType: "application/json; charset=utf-8",
        data:  JSON.stringify(param),
        success: function(data){
          console.log(data);
        },
        error: function(error){
          console.log("Error:");
          console.log(error);
        }
      });
    });

    $(document).on("click","button",function(){

      if($(this).data("text") === "edit"){
        $("#updateForm").show();
        $("#title").val($(this).data("title"));
        $("#body").val($(this).data("body"));
        $("#updateID").text($(this).data("id"));
      }
      else if($(this).data("text") === "editSubmit"){
        var param = {
          id:  $("#id").val(),
          title: $("#title").val(),
          body: $("#body").val()
        };
        console.log(param);
        $.ajax({
          type: "PUT",
          dataType: "json",
          url: "../api/update.php",
          contentType: "application/json; charset=utf-8",
          data:  JSON.stringify(param),
          success: function(data){
            console.log(data);
            $('#alertSuccessUpdate').show();
          },
          error: function(error){
            console.log("Error:");
            console.log(error);
          }
        });      
      }
      else
        { var id = this.id.split("-")[1];
      var param = {
        id: $(this).data("id")
      };
      $.ajax({
        type: "DELETE",
        dataType: "json",
        url: "../api/delete.php",
        contentType: "application/json; charset=utf-8",
        data:  JSON.stringify(param),
        success: function(data){
          console.log(data);
          $('#alertSuccess').show();
        },
        error: function(error){
          console.log("Error:");
          console.log(error);
        }
      });
    }
  });
  });
</script>
<script src="js/sb-admin.min.js"></script>

</body>

</html>
