<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
include_once 'config.php';
include_once 'fortune.php';

$database = new Database();
$db = $database->getConnection();
 
$fortunes = new FortuneCookie($db);
 
$stmt = $fortunes->view();
$num = $stmt->rowCount();

$random_cookie = rand(0, $num-1);

if($num>0){
    
    $fortune_arr=array();
    $fortune_arr["result"]=array();
 
  
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
      
        extract($row);
        $fortune_item=array(
            "id" => $id,
            "fortune" => html_entity_decode($fortune)
        );
        array_push($fortune_arr["result"], $fortune_item);
    }
 
    echo json_encode($fortune_arr["result"][$random_cookie]);
}

else{
    echo json_encode(
        array("message" => "No fortunes found.")
    );
}
?>
