<!DOCTYPE html>
<html lang="en">
<?php
include_once "utils/config.php";
include_once "utils/NewsController.php";
include_once "utils/CommentsController.php";
$database = new Database();
$db = $database->getConnection();

$news = new NewsController($db);

$newslist = $news->view($_GET['id'] ? $_GET['id'] : "");
$newslist->execute();

$comments = new CommentsController($db);

$commentslist = $comments->view($_GET['id'] ? $_GET['id'] : "");
$commentslist->execute();
?>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>WebMag HTML Template</title>

	<link href="assets/https://fonts.googleapis.com/css?family=Nunito+Sans:700%7CNunito:300,600" rel="stylesheet">

	<link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css" />

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<link type="text/css" rel="stylesheet" href="assets/css/style.css" />

</head>
<body>

	<header id="header">

		<div id="nav">

			<div id="nav-fixed">
				<div class="container">

					<div class="nav-logo">
						<a href="assets/index-2.html" class="logo"><img href="assets/img/logo.png" alt=""></a>
					</div>

					<ul class="nav-menu nav navbar-nav">
						<li><a href="index.php">News</a></li>
					</ul>

				</div>
			</div>

		</div>

		<div id="post-header" class="page-header">
			<div class="background-img" style="background: #2F4F4F;"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-10">
						<div class="post-meta">
							<?php
							foreach($newslist as $news){
								?>
								<span class="post-date"><?php echo date("F j, Y, g:i a", strtotime($news['created_at']))?></span>
							</div>
							<h1><?php echo $news['title']; ?></h1>
						</div>
					</div>
				</div>
			</div>

		</header>

		<div class="section">
			<div class="container">

				<div class="row">

					<div class="col-md-8">
						<div class="section-row sticky-container">
							<div class="main-post">
								<h3><?php echo $news['title']; ?></h3>
								<p><?php echo $news['body']; ?></p>
							</div>
						</div>

						<div class="section-row text-center">
							<a href="assets/#" style="display: inline-block;margin: auto;">
								<img class="img-responsive" href="assets/img/ad-2.jpg" alt="">
							</a>
						</div>
					<?php }?>
					<div class="section-row">
						<div class="card card-login mx-auto mt-5">
							<div class="card-header">Comment</div>
							<div class="form-label-group">
								<textarea name="comment" rows="10" cols="30" id="comment"> </textarea>
							</div>
							<a class="btn btn-primary" href="#" id="btnCreateSubmit" data-text="create">Submit</a>
						</div>
					</div>
					<div class="section-title">
						<h2>Comments</h2>
					</div>
					<?php 
					foreach($commentslist as $comments){
						?>
						<div class="post-comments">

							<div class="media">
								<div class="media-left">
									<img class="media-object" href="assets/img/avatar.png" alt="">
								</div>

								<div class="media-body">
									<div class="media-heading">
										<h4>Unkown</h4>
										<span class="time"><?php echo date("F j, Y, g:i a", strtotime($comments['created_at'])) ?></span>
									</div>
									<p><?php echo $comments['comment']; ?></p>
									<button type="button" data-id="<?php echo $comments['id']?>" data-text="delete" class="btn btn-default btnDelete" id="btnDeleteSubmit-<?php echo $comments['id']?>">  
										delete comment
									</button>
									<br><br>
								</div>
							</div>
						</div>
					<?php }?>
				</div>

			</div>

		</div>

	</div>

</div>
<script src="assets/js/jquery.min.js" type="7f869d2906cbba80e143d1b0-text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script href="assets/js/bootstrap.min.js"></script>

<script href="assets/js/main.js" type="3122c46fc42d694ee07516ca-text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$(document).on("click","button",function(){
			if($(this).data("text") === "delete"){
				var id = this.id.split("-")[1];
				var param = {
					id: $(this).data("id")
				};
				$.ajax({
					type: "DELETE",
					dataType: "json",
					url: "api/delete-comment.php",
					contentType: "application/json; charset=utf-8",
					data:  JSON.stringify(param),
					success: function(data){
						console.log(data);
						$('#alertSuccess').show();
					},
					error: function(error){
						console.log("Error:");
						console.log(error);
					}
				});
			}
		});

		$("#btnCreateSubmit").click(function () {
			if($(this).data("text") === "create"){
				var param = {
					comment: $("#comment").val(),
					news_id: <?php echo $_GET['id']; ?>
				};

				$.ajax({
					type: "POST",
					dataType: "json",
					url: "api/create-comment.php",
					contentType: "application/json; charset=utf-8",
					data:  JSON.stringify(param),
					success: function(data){
						console.log(data);
						$('#alertSuccess').show();
					},
					error: function(error){
						console.log("Error:");
						console.log(error);
					}
				});
			}
		});

	});
</script>
</body>

</html>
