<!DOCTYPE html>
<html lang="en">
<?php
include_once "utils/config.php";
include_once "utils/NewsController.php";
$database = new Database();
$db = $database->getConnection();

$newslist = $db->prepare("SELECT * FROM news");
$newslist->execute();
?>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>NEWS</title>

	<link href="assets/https://fonts.googleapis.com/css?family=Nunito+Sans:700%7CNunito:300,600" rel="stylesheet">

	<link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css" />

	<link rel="stylesheet" href="assets/assets/css/font-awesome.min.css">

	<link type="text/css" rel="stylesheet" href="assets/css/style.css" />

	</head>
	<body>

		<header id="header">

			<div id="nav">

				<div id="nav-fixed">
					<div class="container">


						<ul class="nav-menu nav navbar-nav">
							<li><a href="assets/category.html">News</a></li>
						</ul>
					</div>
				</div>
			</div>

		</header>

		<div class="section">

			<div class="container">

				<div class="row">
					<div class="col-md-12">
						<div class="section-title">
							<h2>News</h2>
						</div>
					</div>
					<?php  foreach($newslist as $news){
						?>
						<div class="col-md-4">
							<div class="post">
								<a class="post-img" href="news-post.php?id=<?php echo $news['id']?>"><img src="assets/img/post-3.jpg" alt=""></a>
								<div class="post-body">
									<div class="post-meta">
										<a class="post-category cat-1" href="#"></a>
										<span class="post-date"><?php echo date("F j, Y, g:i a", strtotime($news['created_at'])) ?></span>
									</div>
									<h3 class="post-title"><a href="news-post.php?id=<?php echo $news['id']?>"><?php echo $news['title']?></a></h3>
								</div>
							</div>
						</div>
						<?php
					}
					?>
					<div class="clearfix visible-md visible-lg"></div>

				</div>
			</div>
		</div>

		<footer id="footer">
		</footer>

		<script src="assets/js/jquery.min.js" type="7f869d2906cbba80e143d1b0-text/javascript"></script>
		<script src="assets/js/bootstrap.min.js" type="7f869d2906cbba80e143d1b0-text/javascript"></script>
		<script src="assets/js/main.js" type="7f869d2906cbba80e143d1b0-text/javascript"></script>
</body>

		</html>
