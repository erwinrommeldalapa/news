<?php
// header("Access-Control-Allow-Origin: *");
// header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: DELETE");
// header("Access-Control-Max-Age: 3600");
// header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
 
include_once '../utils/config.php';
include_once '../utils/NewsController.php';
// $method = $_SERVER['REQUEST_METHOD'];

// if($method != 'DELETE')
// {
//     echo '{"message": "Method not allowed"}';
//     die();
// }

$database = new Database();	
$db = $database->getConnection();
 
$news = new NewsController($db);

$data = json_decode(file_get_contents("php://input"));

$news->id = $data->id;
if($news->delete()){
    echo '{';
        echo '"message": "News was deleted."';
    echo '}';
}
 
else{
    echo '{';
        echo '"message": "Unable to delete object."';
    echo '}';
}
?>
