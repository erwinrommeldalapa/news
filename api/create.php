<?php
// header("Access-Control-Allow-Origin: *");
// // header("Content-Type: application/json; charset=UTF-8");
// header("Access-Control-Allow-Methods: POST");
// header("Access-Control-Max-Age: 3600");
// header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../utils/config.php';
include_once '../utils/NewsController.php';
// $method = $_SERVER['REQUEST_METHOD'];

// if($method != 'POST')
// {
//     echo '{"message": "Method not allowed"}';
//     die();
// }

$database = new Database();
$db = $database->getConnection();

$news = new NewsController($db);
$data = json_decode(file_get_contents("php://input"));
$news->title = $data->title;
$news->body = $data->body;
$news->created_at = date('Y-m-d H:i:s');
$news->updated_at = date('Y-m-d H:i:s');
if($news->create()){
	echo '{';
	echo '"message": "news was created."';
	echo '}';
}
else{
	echo '{';
	echo '"message": "Unable to create news."';
	echo '}';
}
?>
