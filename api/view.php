<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
include_once '../utils/config.php';
include_once '../utils/NewsController.php';
 
$database = new Database();
$db = $database->getConnection();

$news = new NewsController($db);

$stmt = $news->view($_GET['id'] ? $_GET['id'] : "");
$num = $stmt->rowCount();

if($num>0){

    $news_arr=array();
    $news_arr["records"]=array();
  
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
      
        extract($row);
 
        $news_item=array(
            "id" => $id,
            "title" => html_entity_decode($title),
            "body" => html_entity_decode($body),
            "created_at" => $created_at,
            "updated_at" => $updated_at,
        );
 
        array_push($news_arr["records"], $news_item);
    }
 
    echo json_encode($news_arr);
}
 
else{
    echo json_encode(
        array("message" => "No news found.")
    );
}
?>
