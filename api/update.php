<?php

header("Access-Control-Allow-Methods: PUT");
 
include_once '../utils/config.php';
include_once '../utils/NewsController.php';

$method = $_SERVER['REQUEST_METHOD'];

if($method != 'PUT')
{
    echo '{"message": "Method not allowed"}';
    die();
}
$database = new Database();
$db = $database->getConnection();

$news = new NewsController($db);
 
$data = json_decode(file_get_contents("php://input"));

$news->id = $data->id;
$news->title = $data->title;
$news->body = $data->body;
$news->updated_at = date('Y-m-d H:i:s');
if($news->update()){
    echo '{';
        echo '"message": "Your news cookie is updated"';
    echo '}';
}
 
else{
    echo '{';
        echo '"message": "Unable to update news"';
    echo '}';
}

?>
