<?php
class CommentsController{

  private $conn;
  private $comments_table = "comments";

  public $id;
  public $comment;
  public $news_id;
  public $created_at;

  public function __construct($db){
    $this->conn = $db;
  }

  function view($id){
    $query = "SELECT * FROM " . $this->comments_table . " WHERE news_id = " . $id;
    $stmt = $this->conn->prepare($query);
    $stmt->execute();

    return $stmt;
  }

  function create(){
    $query = "INSERT INTO
    " . $this->comments_table . "
    SET
    comment=:comment, news_id=:news_id, created_at=:created_at";

    $stmt = $this->conn->prepare($query);

    $this->comment=htmlspecialchars(strip_tags($this->comment));
    $this->news_id=htmlspecialchars(strip_tags($this->news_id));
    $this->created_at=htmlspecialchars(strip_tags($this->created_at));

    $stmt->bindParam(":comment", $this->comment);
    $stmt->bindParam(":news_id", $this->news_id);
    $stmt->bindParam(":created_at", $this->created_at);

    if($stmt->execute()){
      return true;
    }else{
      print_r($stmt->errorInfo());
    }

    return false;
  }

  function delete(){

    $query = "DELETE FROM " . $this->comments_table . " WHERE id = ?";
    $stmt = $this->conn->prepare($query);
    $this->id=htmlspecialchars(strip_tags($this->id));
    $stmt->bindParam(1, $this->id);

    if($stmt->execute()){
      return true;
    }

    return false;

  }

  function update(){
    $query = "UPDATE
    " . $this->comments_table . "
    SET
    title = :title,
    body = :body,
    updated_at = :updated_at
    WHERE
    id = :id";

    $stmt = $this->conn->prepare($query);

    $this->title=htmlspecialchars(strip_tags($this->title));
    $this->body=htmlspecialchars(strip_tags($this->body));
    $this->id=htmlspecialchars(strip_tags($this->id));
    $this->updated_at=htmlspecialchars(strip_tags($this->updated_at));

    $stmt->bindParam(':title', $this->title);
    $stmt->bindParam(':body', $this->body);
    $stmt->bindParam(':id', $this->id);
    $stmt->bindParam(":updated_at", $this->updated_at);

    if($stmt->execute()){
      return true;
    }

    return false;
  }
}