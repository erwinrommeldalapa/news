<?php
class NewsController{

    private $conn;
    private $news_table = "news";
    
    public $id;
    public $title;
    public $body;
    public $created_at;
    public $updated_at;

    public function __construct($db){
        $this->conn = $db;
    }
    
    function view($id){
        if($id != ""){
            $query = "SELECT * FROM " . $this->news_table . " WHERE id = " . $id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }else{
          $query = "SELECT * FROM " . $this->news_table;
          $stmt = $this->conn->prepare($query);
          $stmt->execute();

          return $stmt;

      }
  }

function create(){
    $query = "INSERT INTO
    " . $this->news_table . "
    SET
    title=:title, body=:body, created_at=:created_at, updated_at=:updated_at";

    $stmt = $this->conn->prepare($query);

    $this->title=htmlspecialchars(strip_tags($this->title));
    $this->body=htmlspecialchars(strip_tags($this->body));
    $this->created_at=htmlspecialchars(strip_tags($this->created_at));
    $this->updated_at=htmlspecialchars(strip_tags($this->updated_at));

    $stmt->bindParam(":title", $this->title);
    $stmt->bindParam(":body", $this->body);
    $stmt->bindParam(":created_at", $this->created_at);
    $stmt->bindParam(":updated_at", $this->updated_at);

    if($stmt->execute()){
        return true;
    }else{
     print_r($stmt->errorInfo());
 }

 return false;
}

function delete(){

    $query = "DELETE FROM " . $this->news_table . " WHERE id = ?";
    $stmt = $this->conn->prepare($query);
    $this->id=htmlspecialchars(strip_tags($this->id));
    $stmt->bindParam(1, $this->id);

    if($stmt->execute()){
        return true;
    }

    return false;

}

function update(){
  $query = "UPDATE
  " . $this->news_table . "
  SET
  title = :title,
  body = :body,
  updated_at = :updated_at
  WHERE
  id = :id";

  $stmt = $this->conn->prepare($query);

  $this->title=htmlspecialchars(strip_tags($this->title));
  $this->body=htmlspecialchars(strip_tags($this->body));
  $this->id=htmlspecialchars(strip_tags($this->id));
  $this->updated_at=htmlspecialchars(strip_tags($this->updated_at));

  $stmt->bindParam(':title', $this->title);
  $stmt->bindParam(':body', $this->body);
  $stmt->bindParam(':id', $this->id);
  $stmt->bindParam(":updated_at", $this->updated_at);

  if($stmt->execute()){
    return true;
}

return false;
}
}
